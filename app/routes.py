from app import app, Session
from app.model import *
from flask import jsonify, request
import datetime


@app.route('/themes', methods=['GET', 'POST'])
def themes():
    session = Session()
    if request.method == 'POST':
        q = Theme.from_dict(request.json)
        new_theme = Theme(name=q.name)
        session.add(new_theme)
        session.commit()
        get_new_theme = session.query(Theme).filter_by(name=q.name).first()
        return jsonify(get_new_theme)
    else:
        geted_themes = session.query(Theme).all()
        return jsonify(geted_themes)


@app.route('/themes/<theme_id>', methods=['GET', 'PUT'])
def theme(theme_id):
    session = Session()
    if request.method == 'GET':
        geted_theme = session.query(Theme).filter_by(id=theme_id).first()
        return jsonify(geted_theme)
    else:
        q = Theme.from_dict(request.json)
        session.query(Theme).filter_by(id=theme_id).update({'name': q.name}, synchronize_session="fetch")
        session.commit()
        updated_theme = session.query(Theme).filter_by(id=theme_id).first()
        return jsonify(updated_theme)


@app.route('/themes/<theme_id>/problems', methods=['GET', 'POST'])
def problems_by_theme(theme_id):
    session = Session()
    if request.method == 'GET':
        get_problems_by_theme = session.query(Problem).filter_by(theme_id=theme_id).first()
        return jsonify(get_problems_by_theme)
    else:
        q = Problem.from_dict(request.json)
        new_problem_by_theme = Problem(title=q.title, description=q.description, last_update_time=datetime.datetime.now(), theme_id=theme_id)
        session.add(new_problem_by_theme)
        session.commit()
        get_new_problem_by_theme = session.query(Problem).filter_by(title=q.title).first()
        return jsonify(get_new_problem_by_theme)


@app.route('/problems')
def problems():
    session = Session()
    get_problems = session.query(Problem).all()
    return jsonify(get_problems)


@app.route('/problems/<problem_id>', methods=['GET', 'PUT'])
def problem(problem_id):
    session = Session()
    if request.method == 'GET':
        geted_problem = session.query(Problem).filter_by(id=problem_id).first()
        return jsonify(geted_problem)
    else:
        q = Problem.from_dict(request.json)
        session.query(Problem).filter_by(id=problem_id).update({'title': q.title, 'description': q.description, 'last_update_time': datetime.datetime.now()}, synchronize_session="fetch")
        session.commit()
        updated_problem = session.query(Problem).filter_by(id=problem_id).first()
        return jsonify(updated_problem)


@app.route('/problems/{problem_id}/submissions', methods=['GET', 'POST'])
def submissions(problem_id):
    session = Session()
    get_submissions = session.query(Submission).filter_by(problem_id=problem_id).first()
    if request.method == 'GET':
        return jsonify(get_submissions)
    else:
        q = SubmissionFile.from_dict(request.json)
        new_submission_file = SubmissionFile(name=q.name, file=q.file, submission_id=get_submissions.id)
        session.add(new_submission_file)
        session.commit()
        get_submissions = session.query(Submission).filter_by(problem_id=problem_id).first()
        return jsonify(get_submissions)


@app.route('/problems/{problem_id}/submissions/{submission_id}')
def submission(problem_id, submission_id):
    session = Session()
    get_submissions = session.query(Submission).filter_by(problem_id=problem_id, submission_id=submission_id).first()
    return jsonify(get_submissions)


@app.route('/problems/{problem_id}/submissions/{submission_id}/files')
def files(submission_id):
    session = Session()
    get_files = session.query(SubmissionFile).filter_by(submission_id=submission_id).first()
    return jsonify(get_files)





