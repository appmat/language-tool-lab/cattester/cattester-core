"""create user table

Revision ID: 98ebef731f08
Revises: 8d5bdbf906a4
Create Date: 2020-12-14 19:49:32.512831

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '98ebef731f08'
down_revision = '8d5bdbf906a4'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
