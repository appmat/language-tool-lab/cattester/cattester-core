"""create problem table

Revision ID: ac16a004a4e3
Revises: 
Create Date: 2020-11-05 21:48:35.974271

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ac16a004a4e3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'problem',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('title', sa.String(200), nullable=False),
        sa.Column('description', sa.Text, nullable=False),
        sa.Column('last_update_time', sa.DateTime, nullable=False)
    )


def downgrade():
    op.drop_table('problem')
