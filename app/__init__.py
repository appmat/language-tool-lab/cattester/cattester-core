from flask import Flask
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sqlalchemy as sa

app = Flask(__name__)
app.config.from_json('config.json')

engine = sa.create_engine(app.config['DATABASE_URI'])
Session = sessionmaker(bind=engine)

Base = declarative_base()

from app import routes, model, dto