import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
import confuse

config = confuse.Configuration('ExecutorRunner')
config.set_file('config.yaml')

executor_url = config['paths']['executor_url'].get()

engine = sa.create_engine(config['paths']['database_url'].get())
Session = sessionmaker(bind=engine)
