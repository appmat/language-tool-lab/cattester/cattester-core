"""create test table

Revision ID: affc5c43ad44
Revises: bbd0bd6c4fbe
Create Date: 2020-11-15 23:01:59.371835

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'affc5c43ad44'
down_revision = 'bbd0bd6c4fbe'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'test',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('index', sa.Integer, nullable=False),
        sa.Column('problem_id', sa.Integer, sa.ForeignKey('problem.id', ondelete='CASCADE'), nullable=False),
        sa.Column('input', sa.Text),
        sa.Column('expected_output', sa.Text, nullable=False),
        sa.Column('public', sa.Boolean, default=False)
    )


def downgrade():
    op.drop_table('test')
