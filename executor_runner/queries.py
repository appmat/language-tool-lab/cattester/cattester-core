from core_db import Submission, SubmissionFile, Test


def get_submission_files(submission_id, my_session):
    return my_session.query(SubmissionFile).filter(SubmissionFile.submission_id == submission_id).all()


def get_tests(submission_id, my_session):
    problem_id = my_session.query(Submission.problem_id).filter(Submission.id == submission_id).first()
    return my_session.query(Test).filter(Test.problem_id == problem_id).all()
